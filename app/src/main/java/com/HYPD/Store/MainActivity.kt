package com.HYPD.Store

import alirezat775.lib.carouselview.*
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.HYPD.Store.databinding.ActivityMainBinding
import com.HYPD.Store.listitem.category.ListItemCategory
import com.HYPD.Store.listitem.category.ViewRendererCategory
import com.HYPD.Store.listitem.trending.ListItemTrending
import com.HYPD.Store.listitem.trending.ViewRendererTrending
import com.HYPD.Store.utils.ItemListAdapter


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var carousel: Carousel? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()
        setupTab()
        setDummyCategory()
        setDeals()
        setDummyTrending()
    }

    private fun setDeals() {
        binding.carouselView.layoutManager = CarouselLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        val adapter = DealsAdapter()

        carousel = Carousel(this, binding.carouselView, adapter)
        carousel?.setOrientation(CarouselView.HORIZONTAL, false)
        carousel?.autoScroll(true, 5000, false)
        carousel?.scaleView(false)

        carousel?.add(DealObject.DealModel("Be The Trend"))
        carousel?.add(DealObject.DealModel("Be The Change"))
        carousel?.add(DealObject.DealModel("Be The Trend"))
        carousel?.add(DealObject.DealModel("Be The Change"))
        carousel?.add(DealObject.DealModel("Be The Trend"))

    }


    override fun onDestroy() {
        super.onDestroy()
        carousel?.removeCarouselListener()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupToolbar() {
        /*setActionBar(binding.toolbar)
        binding.toolbar.showOverflowMenu();*/
        binding.toolbar.title = getString(R.string.app_name)
        binding.toolbar.setTitleTextColor(Color.BLACK)
        binding.toolbar.inflateMenu(R.menu.menu_dashboard)
        binding.toolbar.setOnMenuItemClickListener(object : Toolbar.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                if (item.itemId == R.id.menu_cart) {
                    Toast.makeText(
                        this@MainActivity,
                        resources.getString(R.string.cart_toast_msg),
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (item.itemId == R.id.menu_notification) {
                    Toast.makeText(
                        this@MainActivity,
                        resources.getString(R.string.notification_toast_msg),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    // do something
                }
                return false
            }
        })

    }

    fun setupTab(){
        binding.dashboardTabs.addTab(
            binding.dashboardTabs.newTab().setText(resources.getString(R.string.shoping_tab))
        )
        binding.dashboardTabs.addTab(
            binding.dashboardTabs.newTab().setText(resources.getString(R.string.task_tab))
        )
        binding.dashboardTabs.addTab(
            binding.dashboardTabs.newTab().setText(resources.getString(R.string.view_tab))
        )

        for (i in 0 until binding.dashboardTabs.getTabCount()) {
            val tab = (binding.dashboardTabs.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as MarginLayoutParams
            p.setMargins(25, 0, 25, 0)
            tab.requestLayout()
        }
    }
    fun setDummyCategory(){
        val categories = arrayListOf(
            "Shoes",
            "T-Shirt",
            "Jacket"
        )
        val itemListAdapter = ItemListAdapter()

        binding.rvCategories.layoutManager = LinearLayoutManager(
            binding.root.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        binding.rvCategories.adapter = itemListAdapter

        itemListAdapter.addListItem(ListItemCategory("Shoes"), ViewRendererCategory())
        itemListAdapter.addListItem(ListItemCategory("T-Shirt"), ViewRendererCategory())
        itemListAdapter.addListItem(ListItemCategory("Jacket"), ViewRendererCategory())
        itemListAdapter.addListItem(ListItemCategory("Coats"), ViewRendererCategory())
        itemListAdapter.addListItem(ListItemCategory("Watch"), ViewRendererCategory())

    }
    fun setDummyTrending(){
        val categories = arrayListOf(
            "Shoes",
            "T-Shirt",
            "Jacket"
        )
        val itemListAdapter = ItemListAdapter()

        binding.rvTrending.layoutManager = LinearLayoutManager(
            binding.root.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        binding.rvTrending.adapter = itemListAdapter

        itemListAdapter.addListItem(ListItemTrending(true), ViewRendererTrending())
        itemListAdapter.addListItem(ListItemTrending(false), ViewRendererTrending())
        itemListAdapter.addListItem(ListItemTrending(false), ViewRendererTrending())
        itemListAdapter.addListItem(ListItemTrending(true), ViewRendererTrending())
        itemListAdapter.addListItem(ListItemTrending(false), ViewRendererTrending())
        itemListAdapter.addListItem(ListItemTrending(false), ViewRendererTrending())

    }
}