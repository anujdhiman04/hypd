package com.HYPD.Store.utils

/**
 *
 * All list items must return a list item type.
 */
interface ListItemModel {
     fun getViewType(): Int
}