package com.HYPD.Store.utils

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.HYPD.Store.utils.ListItemModel

/**
 * Abstract Methods that each view renderer must implement
 * This allows the bindView and create view holder logic to be implemented in the individual ViewRenderer
 */

abstract class ViewRenderer<M: ListItemModel, H: RecyclerView.ViewHolder>{
    abstract fun bindView(model: M, holder: H, position: Int)

    abstract fun createViewHolder(parent: ViewGroup):H
}