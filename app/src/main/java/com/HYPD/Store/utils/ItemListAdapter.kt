package com.HYPD.Store.utils

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.HYPD.Store.utils.ListItemModel


class ItemListAdapter(
    private var listItems: ArrayList<ListItemModel> = ArrayList(), //Will hold data and view type information
    private var viewRenderers: ArrayList<ViewRenderer<ListItemModel, RecyclerView.ViewHolder>> = ArrayList()
): RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    //Override Methods
    //RecyclerView.ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //viewType = position in the list
        return viewRenderers[viewType].createViewHolder(parent) /*?: ViewRendererDefault().createViewHolder(parent)*/
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun getItemViewType(position: Int): Int {
//        Old Code
//        return listItems[position].getViewType()
        return position
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        Log.d("LIA", "BindView for item $position")
        viewRenderers[position].bindView(
            listItems[position],
            holder,
            position
        )
    }



    fun <L: ListItemModel, V: ViewRenderer<ListItemModel, RecyclerView.ViewHolder>>
            addListItem(item: L, viewRenderer: V){
        listItems.add(item)
        viewRenderers.add(viewRenderer)
//        Old Code
//        if (!viewRenders.containsKey(item.getViewType())){
//            viewRenders[item.getViewType()] = viewRenderer
//        }
        notifyItemChanged(listItems.size - 1)
    }

    fun <L: ListItemModel, V: ViewRenderer<ListItemModel, RecyclerView.ViewHolder>>
            updateListItemAt(item: L, viewRenderer: V, position: Int){
        listItems[position] = item
        viewRenderers[position] = viewRenderer
        notifyItemChanged(position)
    }

    fun getListItemAt(position: Int): ListItemModel{
        return listItems[position]
    }

    fun clear(){
        listItems.clear()
        viewRenderers.clear()
        notifyDataSetChanged()
    }


}