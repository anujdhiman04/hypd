package com.HYPD.Store.listitem.trending

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.HYPD.Store.R
import com.HYPD.Store.listitem.trending.ListItemTrending
import com.HYPD.Store.listitem.trending.ViewHolderTrending
import com.HYPD.Store.utils.ItemListAdapter
import com.HYPD.Store.utils.ListItemModel
import com.HYPD.Store.utils.ViewRenderer

class ViewRendererTrending(
)
    : ViewRenderer<ListItemModel, RecyclerView.ViewHolder>()
{
    //MARK: Properties
    private lateinit var holderTrending: ViewHolderTrending
    private lateinit var trendingModel: ListItemTrending
    private var index: Int = -1

    private val itemListAction = ItemListAdapter()


    override fun bindView(model: ListItemModel, holder: RecyclerView.ViewHolder, position: Int) {
        model as ListItemTrending
        holder as ViewHolderTrending

        trendingModel = model
        holderTrending = holder
        index = position
        
        if (trendingModel.item){
            holderTrending.binding.ibFav.setImageResource(R.drawable.ic_fav_checked)
        }else{
            holderTrending.binding.ibFav.setImageResource(R.drawable.ic_fav)
        }

        holderTrending.binding.ibFav.setOnClickListener {

            trendingModel.item.apply {
                trendingModel.item = !this
            }

            if (trendingModel.item){
                holderTrending.binding.ibFav.setImageResource(R.drawable.ic_fav_checked)
            }else{
                holderTrending.binding.ibFav.setImageResource(R.drawable.ic_fav)
            }
        }

        
    }

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ViewHolderTrending(parent)

    }
}