package com.HYPD.Store.listitem.category

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.HYPD.Store.listitem.category.ListItemCategory
import com.HYPD.Store.utils.ItemListAdapter
import com.HYPD.Store.utils.ListItemModel
import com.HYPD.Store.utils.ViewRenderer

class ViewRendererCategory(
)
    : ViewRenderer<ListItemModel, RecyclerView.ViewHolder>()
{
    //MARK: Properties
    private lateinit var holderCategory: ViewHolderCategory
    private lateinit var categoryModel: ListItemCategory
    private var index: Int = -1

    private val itemListAction = ItemListAdapter()


    override fun bindView(model: ListItemModel, holder: RecyclerView.ViewHolder, position: Int) {
        model as ListItemCategory
        holder as ViewHolderCategory

        categoryModel = model
        holderCategory = holder
        index = position

        holderCategory.binding.tvCategoryName.text = categoryModel.item ?: ""

    }

    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ViewHolderCategory(parent)

    }
}