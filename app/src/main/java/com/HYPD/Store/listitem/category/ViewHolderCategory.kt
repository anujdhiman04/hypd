package com.HYPD.Store.listitem.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.HYPD.Store.databinding.ListItemCategoriesBinding

class ViewHolderCategory(
    val parent: ViewGroup,
    viewBinding: ListItemCategoriesBinding = ListItemCategoriesBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )
): RecyclerView.ViewHolder(viewBinding.root){
    val binding = viewBinding
}