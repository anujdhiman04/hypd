package com.HYPD.Store.listitem.trending

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.HYPD.Store.databinding.ListItemCategoriesBinding
import com.HYPD.Store.databinding.ListItemTrendingBinding

class ViewHolderTrending(
    val parent: ViewGroup,
    viewBinding: ListItemTrendingBinding = ListItemTrendingBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )
): RecyclerView.ViewHolder(viewBinding.root){
    val binding = viewBinding
}