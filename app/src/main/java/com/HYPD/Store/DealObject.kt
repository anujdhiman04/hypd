package com.HYPD.Store

import alirezat775.lib.carouselview.CarouselModel

object DealObject {
    data class DealModel(
        var name: String?
    ): CarouselModel()
}