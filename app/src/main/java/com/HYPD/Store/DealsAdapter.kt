package com.HYPD.Store

import alirezat775.lib.carouselview.CarouselAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_best_deal.view.*

class DealsAdapter : CarouselAdapter() {

    private val EMPTY_ITEM = 0
    private val NORMAL_ITEM = 1

    private var vh: CarouselViewHolder? = null

    override fun getItemViewType(position: Int): Int {
        return NORMAL_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselAdapter.CarouselViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == NORMAL_ITEM) {
            val v = inflater.inflate(R.layout.item_best_deal, parent, false)
            vh = MyViewHolder(v)
            vh as MyViewHolder
        } else {
            //condition if want different layout for empty or other layout
            val v = inflater.inflate(R.layout.item_best_deal, parent, false)
            vh = MyViewHolder(v)
            vh as MyViewHolder
        }
    }

    override fun onBindViewHolder(holder: CarouselViewHolder, position: Int) {
        when (holder) {
            is MyViewHolder -> {
                vh = holder
                val deal = getItems()[position] as DealObject.DealModel
                (vh as MyViewHolder).tvDealName.text = deal.name
            }
            else -> {
                //condition if want different layout for empty or other layout
                vh = holder
                val deal = getItems()[position] as DealObject.DealModel
                (vh as MyViewHolder).tvDealName.text = deal.name
            }
        }
    }

    inner class MyViewHolder(itemView: View) : CarouselViewHolder(itemView) {

        var tvDealName: TextView = itemView.tvDealName

    }


}